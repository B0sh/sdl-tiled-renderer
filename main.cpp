#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <iostream>
#include "RapidXML/rapidxml.hpp"
#include <vector>
#include <string>
#include <fstream>
#include <unordered_map>

#include "tiledlib/TiledTMX.cpp"
#include "renderer.cpp"
#include "timer.cpp"

using namespace std;



int main(int argv, char** args)
{
    Renderer renderer(640, 480, "Tiled Renderer");

    renderer.init();
    renderer.loadTMX("assets/maps/glitch.tmx");

    bool running = true;
    SDL_Event events;
    while (running == true)
    {
        running = renderer.update(events);
        renderer.render();
    }

    SDL_Quit();

    return 1;
}
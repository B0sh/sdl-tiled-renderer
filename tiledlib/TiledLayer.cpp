#pragma once
#include <string>
#include "../RapidXML/rapidxml.hpp"
#include <iostream>
#include <vector>
#include <sstream>

#include "TiledObject.cpp"

using namespace std;

class TiledLayer
{
    public:
        enum class Type : uint8_t
        {
            UNDEFINED,
            TILE_LAYER,
            OBJECT_GROUP
        };

        enum class Encoding : uint8_t
        {
            UNDEFINED,
            CSV, 
            XML, 
            BASE64 
        };

        enum class Compression : uint8_t
        {
            UNCOMPRESSED,
            ZLIB,
            GLIB
        };
        
        TiledLayer(rapidxml::xml_node<>* layerNode);
        void init_tile_layer(rapidxml::xml_node<>* layerNode);
        void init_object_layer(rapidxml::xml_node<>* layerNode);
        void parse_csv_tiles(rapidxml::xml_node<>* dataNode);
        void parse_xml_tiles(rapidxml::xml_node<>* dataNode);
        void parse_base64_tiles(rapidxml::xml_node<>* dataNode);

        int getId();
        string getName();
        bool isVisible();
        vector<vector<unsigned int>> getTileVector();
        Type getType();

    private:
        int m_id;
        string m_name;
        int m_width;
        int m_height;
        double m_opacity = 1.0;
        bool m_visible = 1;

        Type m_type;
        Encoding m_encoding;
        Compression m_compression;

        vector<TiledObject> m_objects;

        // 2d vector
        vector<vector<unsigned int>> m_tiles;
};

bool TiledLayer::isVisible()
{
    return (m_visible == 1 && m_opacity != 0);
}

/*

 <layer id="4" name="ShoeBox Tile Grab" width="230" height="42">
  <data encoding="csv">
    1,1,1,1,1
  </data>
 </layer>
 
 <objectgroup id="3" name="Objects">
  <object id="1" name="mainPlayer" x="2560" y="256" width="16" height="16">
   <properties>
    <property name="frameheight" value="48"/>
    <property name="framewidth" value="48"/>
    <property name="hidden" value="true"/>
    <property name="image" value="character"/>
    <property name="start_z" value="1"/>
   </properties>
  </object>
 </objectgroup>

*/
TiledLayer::TiledLayer(rapidxml::xml_node<>* layerNode)
{
    string name = layerNode->name();

    if (name == "layer")
    {
        m_type = Type::TILE_LAYER;
        TiledLayer::init_tile_layer(layerNode);
    }
    else if (name == "objectgroup")
    {
        m_type = Type::OBJECT_GROUP;
        TiledLayer::init_object_layer(layerNode);
    }
    else
    {
        m_type = Type::UNDEFINED;
    }

    // get opacity and visible if they exist
    // could be on any layer
    if (layerNode->first_attribute("opacity") != 0)
        m_opacity = stod(layerNode->first_attribute("opacity")->value());
    if (layerNode->first_attribute("visible") != 0)
        m_visible = stoi(layerNode->first_attribute("visible")->value());

}

void TiledLayer::init_tile_layer(rapidxml::xml_node<>* layerNode)
{
    rapidxml::xml_node<>* dataNode = layerNode->first_node("data");

    m_id = stoi(layerNode->first_attribute("id")->value());
    m_name = string(layerNode->first_attribute("name")->value());
    m_width = stoi(layerNode->first_attribute("width")->value());
    m_height = stoi(layerNode->first_attribute("height")->value());

    if (dataNode->first_attribute("compression") != 0)
    {
        string compression = dataNode->first_attribute("compression")->value();

        if (compression == "glib")
        {
            m_compression = Compression::GLIB;
        }
        else if (compression == "zlib")
        {
            m_compression = Compression::ZLIB;
        }
        else
        {
            m_compression = Compression::UNCOMPRESSED;
        }
    }
    else
    {
        m_compression = Compression::UNCOMPRESSED;
    }

    if (dataNode->first_attribute("encoding") != 0)
    {
        string encoding = dataNode->first_attribute("encoding")->value();

        if (encoding == "csv")
        {
            m_encoding = Encoding::CSV;
            TiledLayer::parse_csv_tiles(dataNode);
        }
        else if (encoding == "xml")
        {
            m_encoding = Encoding::XML;
            TiledLayer::parse_xml_tiles(dataNode);
        }
        else if (encoding == "base64")
        {
            m_encoding = Encoding::BASE64;
            TiledLayer::parse_base64_tiles(dataNode);
        }
        else
        {
            m_encoding = Encoding::UNDEFINED;
        }
    }
    else
    {
        m_encoding = Encoding::UNDEFINED;
    }

}

void TiledLayer::parse_csv_tiles(rapidxml::xml_node<>* dataNode)
{
    string str = dataNode->value();
    stringstream ss(str);

    int x = 0;
    int y = 0;
    vector<vector<unsigned int>> tiles(m_height, vector<unsigned int>(m_width));

    for (int i; ss >> i;) 
    {
        tiles[y][x] = i;
        if (ss.peek() == ',')
            ss.ignore();

        x++;
        if (x == m_width)
        {
            y++;
            x = 0;
        }
    }

    m_tiles = tiles; 

    // print tiles
    // for (std::size_t i = 0; i < m_tiles.size(); i++)
    // {
    //     for (std::size_t u = 0; u < m_tiles[i].size(); u++)
    //         cout << m_tiles[i][u] << " . ";
    //     cout << endl;
    // }

}

void TiledLayer::parse_xml_tiles(rapidxml::xml_node<>* dataNode)
{
    throw "TiledLayer: Parsing XML files is not implimented.";   
}

void TiledLayer::parse_base64_tiles(rapidxml::xml_node<>* dataNode)
{
    throw "TiledLayer: Parsing Base64 files is not implimented.";   
}

void TiledLayer::init_object_layer(rapidxml::xml_node<>* layerNode)
{
    rapidxml::xml_node<>* dataNode = layerNode->first_node("data");

    m_id = stoi(layerNode->first_attribute("id")->value());
    m_name = string(layerNode->first_attribute("name")->value());
    
    rapidxml::xml_node<>* currentNode = layerNode->first_node("object");

    while (currentNode != nullptr)
    {
        m_objects.push_back(TiledObject(currentNode));
        currentNode = currentNode->next_sibling("object");
    }

}

int TiledLayer::getId()
{
    return m_id;
}

string TiledLayer::getName()
{
    return m_name;
}

vector<vector<unsigned int>> TiledLayer::getTileVector()
{
    return m_tiles;
}

TiledLayer::Type TiledLayer::getType()
{
    return m_type;
}
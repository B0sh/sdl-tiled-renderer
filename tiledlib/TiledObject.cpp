#pragma once
#include <string>
#include "../RapidXML/rapidxml.hpp"
#include <iostream>
#include <unordered_map>

using namespace std;

class TiledObject
{
    public:
        TiledObject(rapidxml::xml_node<>*);
    private:
        int m_id;
        string m_name;
        int m_x;
        int m_y;
        int m_width;
        int m_height;

        unordered_map<string, string> m_properties;
};

/*

Content to parse:

  <object id="1" name="mainPlayer" x="2560" y="256" width="16" height="16">
   <properties>
    <property name="frameheight" value="48"/>
    <property name="framewidth" value="48"/>
    <property name="hidden" value="true"/>
    <property name="image" value="character"/>
    <property name="start_z" value="1"/>
   </properties>
  </object>

*/

TiledObject::TiledObject(rapidxml::xml_node<>* tilesetNode)
{
    // load tileset information
    m_id = stoi(tilesetNode->first_attribute("id")->value());
    m_name = tilesetNode->first_attribute("name")->value();
    m_x = stoi(tilesetNode->first_attribute("x")->value());
    m_y =  stoi(tilesetNode->first_attribute("y")->value());
    m_width = stoi(tilesetNode->first_attribute("width")->value());
    m_height = stoi(tilesetNode->first_attribute("height")->value());

    rapidxml::xml_node<>* properties = tilesetNode->first_node("properties");

    if (properties != nullptr)
    {
        // traverse the property nodes
        rapidxml::xml_node<>* currentNode = properties->first_node("property");

        while (currentNode != nullptr)
        {
            cout << currentNode->first_attribute("name")->value() << endl;

            // if value property doesn't exist, its innnerHTML
            if (currentNode->first_attribute("value"))
            {
                m_properties[currentNode->first_attribute("name")->value()]
                    = currentNode->first_attribute("value")->value();
            }
            else
            {
                m_properties[currentNode->first_attribute("name")->value()]
                    = currentNode->value();
            }
            
			currentNode = currentNode->next_sibling("property");
        }
    }
}


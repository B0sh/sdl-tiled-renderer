#pragma once
#include <iostream>
#include "../RapidXML/rapidxml.hpp"
#include <array>
#include <vector>
#include <string>
#include <regex>
#include <fstream>
#include <unordered_map>
#include <typeinfo>
#include "TiledTileset.cpp"
#include "TiledLayer.cpp"

using namespace std;

class TiledTMX
{
    public:
        TiledTMX(const char*);
        ~TiledTMX();
        void loadMapSettings(rapidxml::xml_node<>* map);
        void loadProperties(rapidxml::xml_node<>* map);
        void loadTilesets(rapidxml::xml_node<>* map);
        void loadLayers(rapidxml::xml_node<>* map);

        string getProperty(string);
        TiledTileset& getTilesetFromTileID(int);

        int getWidth();
        int getHeight();
        int getTileWidth();
        int getTileHeight();
        vector<TiledTileset>& getTilesets();
        vector<TiledLayer>& getLayers();

        int getBGR();
        int getBGG();
        int getBGB();
        
        void print(); 
    private:
        int m_width;
        int m_height;
        int m_tilewidth;
        int m_tileheight;

        int m_bg_r;
        int m_bg_g;
        int m_bg_b;

        unordered_map<string, string> m_properties;
        vector<TiledTileset> m_tilesets;
        vector<TiledLayer> m_layers;

        rapidxml::xml_document<> doc;

        const char* m_filename;
};

TiledTMX::TiledTMX(const char* tmx_filename)
{
    // https://stackoverflow.com/questions/2808022/how-to-parse-an-xml-file-with-rapidxml
    ifstream myfile(tmx_filename);

    /* "Read file into vector<char>"  See linked thread above*/
    vector<char> buffer((istreambuf_iterator<char>(myfile)), istreambuf_iterator<char>( ));
    buffer.push_back('\0');

    // buffer[0] ref contains the contents of the file
    // cout<<&buffer[0]<<endl; 

    doc.parse<0>(&buffer[0]);

    m_filename = tmx_filename;

    rapidxml::xml_node<>* map = doc.first_node("map");

    loadMapSettings(map);
    loadProperties(map);
    loadTilesets(map);
    loadLayers(map);
}

TiledTMX::~TiledTMX()
{
    
}

string TiledTMX::getProperty(string a)
{
    return m_properties[a];
}

int TiledTMX::getWidth()
{
    return m_width;
}

int TiledTMX::getHeight()
{
    return m_height;
}

int TiledTMX::getTileWidth()
{
    return m_tilewidth;
}

int TiledTMX::getTileHeight()
{
    return m_tileheight;
}

vector<TiledTileset>& TiledTMX::getTilesets()
{
    return m_tilesets;
}

vector<TiledLayer>& TiledTMX::getLayers()
{
    return m_layers;
}

int TiledTMX::getBGR()
{
    return m_bg_r;
}

int TiledTMX::getBGG()
{
    return m_bg_g;
}

int TiledTMX::getBGB()
{
    return m_bg_b;
}

TiledTileset& TiledTMX::getTilesetFromTileID(int tileID)
{
    for (auto &tileset : m_tilesets)
    {
        if (tileset.isTileInTileset(tileID))
        {
            return tileset;
        }
    }
}

void TiledTMX::loadMapSettings(rapidxml::xml_node<>* map)
{
    m_width = stoi(map->first_attribute("width")->value());
    m_height = stoi(map->first_attribute("height")->value());
    m_tilewidth = stoi(map->first_attribute("tilewidth")->value());
    m_tileheight = stoi(map->first_attribute("tileheight")->value());

    // vector<Uint8> m_backgroundcolor(3, 25);
    // background color must be converted from hex string to unsigned long
    if (map->first_attribute("backgroundcolor") != 0)
    {
        string color = map->first_attribute("backgroundcolor")->value();

        // https://codereview.stackexchange.com/questions/172482/convert-hex-color-string-to-sdl-color
        regex pattern("#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})");
        smatch match;
        if (regex_match(color, match, pattern))
        {
            // m_backgroundcolor[0] = stoul(match[1].str(), nullptr, 16); // r
            // m_backgroundcolor[1] = stoul(match[2].str(), nullptr, 16); // g
            // m_backgroundcolor[2] = stoul(match[3].str(), nullptr, 16); // b
            m_bg_r = stoi(match[1].str(), nullptr, 16); // r
            m_bg_g = stoi(match[2].str(), nullptr, 16); // g
            m_bg_b = stoi(match[3].str(), nullptr, 16); // b
        }
    }
    else
    {
        // m_backgroundcolor[0] = 0;
        // m_backgroundcolor[1] = 0;
        // m_backgroundcolor[2] = 0;
    }

}

void TiledTMX::loadProperties(rapidxml::xml_node<>* map)
{
    rapidxml::xml_node<>* properties = map->first_node("properties");

    if (properties != nullptr)
    {
        // traverse the property nodes
        rapidxml::xml_node<>* currentNode = properties->first_node("property");

        while (currentNode != nullptr)
        {
            m_properties[currentNode->first_attribute("name")->value()]
                 = currentNode->first_attribute("value")->value();

			currentNode = currentNode->next_sibling("property");
        }
    }
}

void TiledTMX::loadTilesets(rapidxml::xml_node<>* map)
{
    rapidxml::xml_node<>* currentNode = map->first_node("tileset");

    while (currentNode != nullptr)
    {
        // insert at the end of the vector
        m_tilesets.push_back(TiledTileset(currentNode));

        currentNode = currentNode->next_sibling("tileset");
    }
    
}

void TiledTMX::loadLayers(rapidxml::xml_node<>* map)
{
    rapidxml::xml_node<>* currentNode = map->first_node();

    // loop through all nodes and take only ones that are layers
    while (currentNode != nullptr)
    {
        // this part was extremely frustrating
        // i guess i had to reinitialize the string or something 
        // since inlining currentNode->name() didn't work at all
        string name = currentNode->name();
        
        if (name == "layer" || name == "objectgroup") 
        {
            m_layers.push_back(TiledLayer(currentNode));
        }

        currentNode = currentNode->next_sibling();
    }
}


void TiledTMX::print()
{
    cout << "--- " << m_filename << " :: TiledTMX Debug---" << endl << endl;

    cout << "Width: " << m_width << endl;
    cout << "Height: " << m_height << endl;
    cout << "Tilewidth: " << m_tilewidth << endl;
    cout << "Tileheight: " << m_tileheight << endl;
    cout << "Background Color: #" << hex << (int)m_bg_r << hex << (int)m_bg_g << hex << (int)m_bg_b << endl;

    cout << endl << "Properties:" << endl;
    for (const auto & [ key, value ] : m_properties)
    {
        cout << key << ": " << value << endl;
    }

    cout << endl << "Tilesets:" << endl;
    for (auto & t : m_tilesets)
    {
        cout << t.getName() << ": " << t.getImagePath() << endl;
    }

    cout << endl << "Layers:" << endl;
    for (auto & l : m_layers)
    {
        cout << l.getId() << ": " << l.getName() << endl;
    }

}
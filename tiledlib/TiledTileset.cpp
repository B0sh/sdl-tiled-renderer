#pragma once
#include <string>
#include "../RapidXML/rapidxml.hpp"
#include <iostream>

using namespace std;

class TiledTileset
{
    public:
        TiledTileset(rapidxml::xml_node<>*);
        string getName();
        string getImagePath();
        int getFirstGID();
        int getColumns();
        bool isTileInTileset(int);

    private:
        int m_firstgid;
        string m_name;
        int m_tilewidth;
        int m_tileheight;
        int m_tilecount;
        int m_columns;

        string m_imagepath;
        int m_imagewidth;
        int m_imageheight;
};

/*

Content to parse:

<tileset firstgid="353" name="font" tilewidth="16" tileheight="16" tilecount="128" columns="16">
  <image source="../Tilesets/font.png" width="256" height="128"/>
</tileset>

*/

TiledTileset::TiledTileset(rapidxml::xml_node<>* tilesetNode)
{
    // load tileset information
    m_firstgid = stoi(tilesetNode->first_attribute("firstgid")->value());
    m_name = tilesetNode->first_attribute("name")->value();
    m_tilewidth = stoi(tilesetNode->first_attribute("tilewidth")->value());
    m_tileheight =  stoi(tilesetNode->first_attribute("tileheight")->value());
    m_tilecount = stoi(tilesetNode->first_attribute("tilecount")->value());
    m_columns = stoi(tilesetNode->first_attribute("columns")->value());

    rapidxml::xml_node<>* imageNode = tilesetNode->first_node("image");

    if (imageNode != nullptr)
    {
        m_imagepath = imageNode->first_attribute("source")->value();
        m_imagewidth = stoi(imageNode->first_attribute("width")->value());
        m_imageheight = stoi(imageNode->first_attribute("height")->value());
    }
}

bool TiledTileset::isTileInTileset(int tileID)
{
    return (m_firstgid <= tileID && (m_firstgid + m_tilecount) > tileID);
}

string TiledTileset::getName()
{
    return m_name;
}

string TiledTileset::getImagePath()
{
    return m_imagepath;
}

int TiledTileset::getFirstGID()
{
    return m_firstgid;
}

int TiledTileset::getColumns()
{
    return m_columns;
}
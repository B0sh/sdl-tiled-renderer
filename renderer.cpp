#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <cstddef>  
#include <fstream>
#include <typeinfo>
#include <unordered_map>
#include "tiledlib/TiledTMX.cpp"
#include "timer.cpp"

using namespace std;

class Renderer
{
    public:
        Renderer(const int width, const int height, string window_title);
        ~Renderer();
        bool init();
        bool update(SDL_Event &events);
        bool loadTMX(const char* tmx_filename);
        void render();
    private:
        TiledTMX* m_map;
        int m_screen_width;
        int m_screen_height;

        int m_tile_offset_x;
        int m_tile_offset_y;

        string m_window_title;

        SDL_Window* m_window = NULL;
        SDL_Renderer* m_renderer = NULL;

        unordered_map<string, SDL_Texture*> m_tileset_textures;
};

Renderer::Renderer(const int width, const int height, string window_title)
{
    m_screen_width = width;
    m_screen_height = height;
    m_window_title = window_title;

    m_tile_offset_x = 0;
    m_tile_offset_y = 0;
}

Renderer::~Renderer()
{
    // impliment destructor
}

bool Renderer::init()
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		return false;
	}

    Uint32 windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    m_window = SDL_CreateWindow(m_window_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_screen_width, m_screen_height, windowFlags);

    //Create window
    if (m_window == NULL)
    {
        printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
        return false;
    }

    // create the renderer for the window
    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    if (m_renderer == NULL)
    {
        printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
        return false;
    }

    // Initialize renderer color
    SDL_SetRenderDrawColor(m_renderer, 0xFF, 0xFF, 0xFF, 0xFF);

    // Initialize PNG Loading
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags))
    {
        printf("SDL image could not initialize! SDL_image error: %s \n", IMG_GetError());
        return false;
    }

    if (TTF_Init() == -1)
    {
        printf("SDL_ttf could not initialize! SDL_ttf Error: %s \n", TTF_GetError());
        return false;
    }

    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
    {
        printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
        return false;
    }

    return true;
}

// will probably move at some point
bool Renderer::update(SDL_Event &events)
{
    while (SDL_PollEvent(&events))
    {
        switch (events.type)
        {
            case SDL_WINDOWEVENT:
                switch (events.window.event)
                {
                    case SDL_WINDOWEVENT_CLOSE:
                        return false;
                        break;
                }
                break;
            case SDL_KEYDOWN:
                switch (events.key.keysym.sym)
                {
                    case SDLK_w:
                        m_tile_offset_y--;
                        break;
                    case SDLK_a:
                        m_tile_offset_x--;
                        break;
                    case SDLK_s:
                        m_tile_offset_y++;
                        break;
                    case SDLK_d:
                        m_tile_offset_x++;
                        break;
                }
                break;
            case SDL_QUIT:
                return false;
                break;
        }
    }
    return true;
}

void Renderer::render()
{
    Timer timer;
    timer.start();

    int r = m_map->getBGR();
    int b = m_map->getBGB();
    int g = m_map->getBGG();

    SDL_SetRenderDrawColor(m_renderer, r, g, b, 0);
    
    SDL_RenderClear(m_renderer);

    int tileID = 0;

    int tilewidth = m_map->getTileWidth();
    int tileheight = m_map->getTileHeight();
    int width = m_map->getWidth();
    int height = m_map->getHeight();

    int x_min = m_tile_offset_x;
    int y_min = m_tile_offset_y;
    if (x_min < 0) x_min = 0;
    if (y_min < 0) y_min = 0;

    int x_max = m_screen_width / tilewidth + x_min;
    int y_max = m_screen_height / tileheight + y_min;

    m_tile_offset_x ++;

    for (auto &layer : m_map->getLayers())
    {

        if (!layer.isVisible() || layer.getType() != TiledLayer::Type::TILE_LAYER)
            continue;

        cout << "R:" << layer.getName() << " ";

        for (int j = y_min; j < y_max; ++j)
        {
            for (int i = x_min; i < x_max; ++i)
            {
                // get the tile at current position
                tileID = layer.getTileVector()[j][i];
                
                // only render if it is an actual tile (tileID = 0 means no tile / don't render anything here)
                if (tileID > 0)
                {
                    TiledTileset tileset = m_map->getTilesetFromTileID(tileID);

                    int tilesetTileID = tileID - tileset.getFirstGID();

                    SDL_Rect srcrect = { 
                        (tilesetTileID % tileset.getColumns()) * tilewidth, 
                        (tilesetTileID / tileset.getColumns()) * tileheight, 
                        tilewidth, 
                        tileheight 
                    };
                    SDL_Rect dstrect = { 
                        (i - x_min) * tilewidth, 
                        (j - y_min) * tileheight,
                        tilewidth, 
                        tileheight 
                    };

                    SDL_RenderCopy(m_renderer, m_tileset_textures[tileset.getName()], &srcrect, &dstrect);
                }
            }
        }
        // break;
    }

    SDL_RenderPresent(m_renderer);

    cout << "Frame Rendered! (" << timer.getTicks() / 1000.f << "s)" << endl;
}

bool Renderer::loadTMX(const char* tmx_filename)
{
    m_map = new TiledTMX(tmx_filename);

    // get the directory path of the tiled path
    size_t found = string(tmx_filename).find_last_of
    ("/\\");
    string path = string(tmx_filename).substr(0, found+1);

    // load tileset images
    for (auto &t : m_map->getTilesets())
    {
        string image_path = path + t.getImagePath();

        m_tileset_textures[t.getName()] = IMG_LoadTexture(m_renderer, image_path.c_str());

        if (m_tileset_textures[t.getName()] == NULL)
        {
            printf("Unable to load image %s! SDL_image Error: %s\n", image_path.c_str(), IMG_GetError());

            return false;
        }

    }

    m_map->print();

    return true;
}